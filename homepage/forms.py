from django import forms
from .models import Forms1, Forms2, Forms3
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User

class matkulForm(forms.Form):

    mata_kuliah = forms.CharField(widget=forms.TextInput(attrs={
        'class': 'form-control',
        'placeholder': 'Mata Kuliah',
        'type' : 'text',
        'required': True,
    }))

    dosen = forms.CharField(widget=forms.TextInput(attrs={
        'class': 'form-control',
        'placeholder' : 'Nama Dosen',
        'type' : 'text',
        'required': True,
    }))

    sks = forms.IntegerField(widget=forms.NumberInput(attrs={
        'class': 'form-control',
        'placeholder':'Jumlah SKS',
        'type':'number',
    }))


    tahun = forms.IntegerField(widget=forms.NumberInput(attrs={
        'class': 'form-control',
        'type' : 'number',
        'placeholder': 'Tahun',
        'required': True,
    }))

    kelas = forms.CharField(widget=forms.TextInput(attrs={
        'class': 'form-control',
        'type' : 'text',
        'placeholder': 'Kelas',
        'required': True,
    }))

class Forms_band(forms.ModelForm):
    class Meta:
        model = Forms1
        fields = ['Nama']
        

class Forms_nongkrong(forms.ModelForm):
    class Meta:
        model = Forms2
        fields = ['Nama']

class Forms_mabar(forms.ModelForm):
    class Meta:
        model = Forms3
        fields = ['Nama']

class loginForm(forms.Form):
    username = forms.CharField(widget=forms.TextInput(attrs={
        'class': 'form-control',
        'type' : 'text',
        'placeholder': "Enter your username",
        'required': True,
    }))
    password = forms.CharField(widget=forms.PasswordInput(attrs={
        'class': 'form-control',
        'type' : 'password',
        'placeholder': "Enter your password",
        'required': True,
    }))


class registerForm(UserCreationForm):
    class Meta:
        model = User
        fields = [
            'username', 'email', 'password1','password2',
        ]
    
