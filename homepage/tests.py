from django.test import TestCase, Client
from django.urls import reverse
from .models import Forms1, Forms2, Forms3
from .forms import Forms_band, Forms_nongkrong, Forms_mabar, loginForm, registerForm
from django.contrib.auth import authenticate, login, logout
# Create your tests here.
class UnitTest(TestCase):
    def test_root_url_status_200(self):
        response = self.client.get(reverse('homepage:kegiatan'))
        self.assertEqual(response.status_code, 200)
    
    def test_template_yang_digunakan_dari_halaman_kegiatan(self):
        response = self.client.get(reverse('homepage:kegiatan'))
        self.assertTemplateUsed(response, 'Kegiatan.html')
    
    def test_isi_dari_halaman_kegiatan(self):
        response = self.client.get(reverse('homepage:kegiatan'))
        isi = response.content.decode('utf8')
        self.assertIn("Ngeband", isi)
        self.assertIn("Nongkrong", isi)
        self.assertIn("Mabar", isi)
        self.assertIn("<input type='submit' class='btn btn-primary' name = 'btn_form_band' value='Tambah Peserta'/>", isi)
        self.assertIn("<input type='submit' class='btn btn-primary' name = 'btn_form_nongkrong' value='Tambah Peserta'/>", isi)
        self.assertIn("<input type='submit' class='btn btn-primary' name = 'btn_form_mabar' value='Tambah Peserta'/>", isi)
        self.assertIn('<form method="POST" action="" id="forms_band" style="margin-top: 20px;">', isi)
        self.assertIn('<form method="POST" action="" id="forms_nongkrong" style="margin-top: 20px;">', isi)
        self.assertIn('<form method="POST" action="" id="forms_mabar" style="margin-top: 20px;">', isi)
        self.assertIn("Nama", isi)
    
    def test_models_dari_halaman_kegiatan(self):
        Forms1.objects.create(Nama='abc')
        n_band = Forms1.objects.all().count()
        self.assertEqual(n_band,1)
        Forms1.objects.create(Nama='abc')
        n_nongkrong = Forms2.objects.all().count()
        self.assertEqual(n_nongkrong,0)
        Forms1.objects.create(Nama='abc')
        n_mabar = Forms3.objects.all().count()
        self.assertEqual(n_mabar,0)
    
    def test_form_band_post_valid(self):
        response_post = self.client.post('/kegiatan', {'Nama':'abc', 'btn_form_band':'Tambah Peserta'})
        self.assertEqual(response_post.status_code,200)
        forms_band = Forms_band(data={'Nama':'abc'})
        self.assertTrue(forms_band.is_valid())
        self.assertEqual(forms_band.cleaned_data['Nama'],'abc')

    def test_form_nongkrong_post_valid(self):
        response_post = self.client.post('/kegiatan', {'Nama':'abc', 'btn_form_nongkrong':'Tambah Peserta'})
        self.assertEqual(response_post.status_code,301)
        forms_nongkrong = Forms_nongkrong(data={'Nama':'abc'})
        self.assertTrue(forms_nongkrong.is_valid())
        self.assertEqual(forms_nongkrong.cleaned_data['Nama'],'abc')

    def test_form_band_post_valid(self):
        response_post = self.client.post('/kegiatan', {'Nama':'abc', 'btn_form_mabar':'Tambah Peserta'})
        self.assertEqual(response_post.status_code,301)
        forms_mabar = Forms_mabar(data={'Nama':'abc'})
        self.assertTrue(forms_mabar.is_valid())
        self.assertEqual(forms_mabar.cleaned_data['Nama'],'abc')

    def test_url (self):
        response = Client().get("/caribuku/")
        self.assertEqual(response.status_code,200)

    def test_template(self):
        response = Client().get('/caribuku/')
        self.assertTemplateUsed(response,'Search.html')

    def test_url_json(self):
        response = Client().get('/caribuku/data')
        self.assertEqual(response.status_code,200)

    def test_isi_html (self):
        response = self.client.get(reverse('homepage:caribuku'))
        isi = response.content.decode('utf8')
        self.assertIn("Cari Buku", isi)
        self.assertIn("No", isi)
        self.assertIn("Cover", isi)
        self.assertIn("Judul Buku", isi)
        self.assertIn("Author", isi)

    def test_page_login(self):
        response = self.client.get('/login/')
        self.assertEqual(response.status_code,200)
    
    def test_page_register(self):
        response = self.client.get('/register/')
        self.assertEqual(response.status_code,200)

    def template_used_login(self):
        response = self.client.get(reverse('homepage:login'))
        self.assertTemplateUsed(response,'login.html')

    def template_used_register(self):
        response = self.client.get(reverse('homepage:register'))
        self.assertTemplateUsed(response,'register.html')

    def test_form_login_valid(self):
        form_login = loginForm(data={'username':"xxxx" , 'password':"abcdabcd"})
        self.assertTrue(form_login.is_valid())
        self.assertEqual(form_login.cleaned_data['username'],"xxxx")
        self.assertEqual(form_login.cleaned_data['password'],"abcdabcd")
        user = authenticate(username = "xxxx" , password = "abcdabcd")
        self.client.login()
        response = Client().get('/login/')
        self.assertEqual(response.status_code,200)

    def test_form_register_valid(self):
        form_register = registerForm(data={
            'username':"xxxx" ,
            'email' : 'bbbbb@yahoo.com', 
            'password1':"abcdabcd",
            'password2':"abcdabcd"
        })
        self.assertTrue(form_register.is_valid())
        self.assertEqual(form_register.cleaned_data['username'],"xxxx")
        self.assertEqual(form_register.cleaned_data['email'],"bbbbb@yahoo.com")
        self.assertEqual(form_register.cleaned_data['password1'],"abcdabcd")
        self.assertEqual(form_register.cleaned_data['password2'],"abcdabcd")

    def test_logout_valid(self):
        self.client.logout
        response = Client().get('/login/')
        form_login = loginForm()
        self.assertEqual(response.status_code,200)

