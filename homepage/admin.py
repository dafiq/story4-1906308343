from django.contrib import admin
from .models import Matkul, Forms1, Forms2, Forms3
# Register your models here.
admin.site.register(Matkul)
admin.site.register(Forms1)
admin.site.register(Forms2)
admin.site.register(Forms3)
