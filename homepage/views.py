from django.shortcuts import render, redirect
from .models import Matkul, Forms1, Forms2, Forms3
from .forms import matkulForm, Forms_band, Forms_mabar, Forms_nongkrong, loginForm, registerForm
from django.http import JsonResponse
from django.contrib.auth import authenticate, login, logout
from django.contrib import messages
import json
import requests

# Create your views here.
def homepage(request):
    return render(request, 'Homepage.html')

def riwayat(request):
    return render(request, 'RiwayatPendidikan.html')

def hobi(request):
    return render(request, 'Hobi.html')

def pengalaman(request):
    return render(request, 'Pengalaman.html')

def karya(request):
    return render(request, 'Karya.html')

def contact(request):
    return render(request, 'Contact.html')

def matkulpage(request):
    if request.method == "POST":
        form = matkulForm(request.POST)
        if form.is_valid():
            formkuliah = Matkul()
            formkuliah.mata_kuliah = form.cleaned_data['mata_kuliah']
            formkuliah.dosen = form.cleaned_data['dosen']
            formkuliah.sks = form.cleaned_data['sks']
            formkuliah.tahun = form.cleaned_data['tahun']
            formkuliah.kelas = form.cleaned_data['kelas']
            formkuliah.save()
            
        return redirect('/matkulpage')
    else:
        matakuliah = Matkul.objects.all()
        form = matkulForm()
        response = {"matakuliah":matakuliah, 'form' : form}
        return render(request,'Matakuliah.html',response)
    
def hapusmatkul(request,pk):
    if request.method == 'POST':
        Matkul.objects.filter(pk=pk).delete()
        matakuliah = Matkul.objects.all()
        form = matkulForm() 
        form = matkulForm(request.POST)
        if form.is_valid():
            formkuliah = Matkul()
            formkuliah.mata_kuliah = form.cleaned_data['mata_kuliah']
            formkuliah.dosen = form.cleaned_data['dosen']
            formkuliah.sks = form.cleaned_data['sks']
            formkuliah.tahun = form.cleaned_data['tahun']
            formkuliah.kelas = form.cleaned_data['kelas']
            formkuliah.save()
        return redirect('/matkulpage')
    else:
        return redirect('/matkulpage')

def kegiatan(request):
    if request.method == 'POST' and 'btn_form_band' in request.POST:
        forms_band = Forms_band(request.POST)
        if forms_band.is_valid():
            forms_band.save()
    elif request.method == 'POST' and 'btn_form_nongkrong' in request.POST:
        forms_nongkrong = Forms_nongkrong(request.POST)
        if forms_nongkrong.is_valid():
            forms_nongkrong.save()
    elif request.method == 'POST' and 'btn_form_mabar' in request.POST:
        forms_mabar = Forms_mabar(request.POST)
        if forms_mabar.is_valid():
            forms_mabar.save()
    
    forms_band = Forms_band()
    forms_nongkrong = Forms_nongkrong()
    forms_mabar = Forms_mabar()
    peserta_band = Forms1.objects.all()
    peserta_nongkrong = Forms2.objects.all()
    peserta_mabar = Forms3.objects.all()
    context = {
        'list_peserta_band':peserta_band,
        'list_peserta_nongkrong':peserta_nongkrong,
        'list_peserta_mabar':peserta_mabar,
        'forms_band':forms_band,
        'forms_nongkrong':forms_nongkrong,
        'forms_mabar':forms_mabar,
        }
    return render(request, 'Kegiatan.html', context)

def prestasi(request):
    return render(request, 'Prestasi.html')

def caribuku(request):
    response = {}
    return render(request,'Search.html',response)

def data(request):
    try:
        tes = request.GET['q']
    except:
        tes = "a"
    
    json_read = requests.get('https://www.googleapis.com/books/v1/volumes?q=' + tes).json()

    return JsonResponse(json_read)

def login_view(request):
    form = loginForm()
    flag = False
    if request.method == "POST":
        form = loginForm(request.POST)
        if form.is_valid():
            uname = form.cleaned_data['username']
            passw = form.cleaned_data['password']
            user = authenticate(request, username=uname, password = passw)
            if user is not None:
                login(request, user)
                flag = True
                response = {'form':form,'user':user,'flag':flag}
                return redirect('/login')
            else:
                response = {'form':form,'user':user,'flag':flag}
                return redirect('/login')
    else:
        response = {'form':form,'flag':flag}
        return render(request,'login.html',response)

def register(request):
    if request.user.is_authenticated:
	    return redirect ('/register')
    else:
        form = registerForm()
        if request.method == 'POST':
            form = registerForm(request.POST)
            if form.is_valid():
                form.save()
                user = form.cleaned_data.get('username')
                messages.success(request, 'Account was created for ' + user)

                return redirect('/login')
			

        context = {'form':form}
        return render(request, 'register.html', context)

def logout_view(request):
    logout(request)
    form = loginForm()
    response = {'form':form}
    return redirect('/login')
