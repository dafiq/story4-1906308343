from django.urls import path
from .views import homepage
from .views import riwayat
from .views import hobi
from .views import pengalaman
from .views import karya
from .views import contact
from .views import matkulpage
from .views import hapusmatkul
from .views import kegiatan
from .views import prestasi
from .views import caribuku
from .views import data
from .views import login_view
from .views import register
from .views import logout_view
app_name = 'homepage'

urlpatterns = [
    path('', homepage, name='homepage'),
    path('riwayatpendidikan/', riwayat, name='riwayat'),
    path('hobi/', hobi, name='hobi'),
    path('pengalaman/', pengalaman, name="pengalaman"),
    path('karya/', karya, name="karya"),
    path('contact/', contact, name="contact"),
    path('matkulpage/', matkulpage, name='matkulpage'),
    path('<int:pk>', hapusmatkul, name='delete'),
    path('kegiatan/', kegiatan, name='kegiatan'),
    path('prestasi/', prestasi, name='prestasi'),
    path('caribuku/', caribuku, name='caribuku'),
    path('caribuku/data', data, name='data'),
    path('login/', login_view, name='login'),
    path('logout/', logout_view, name='logout'),
    path('register/', register, name='register')

]